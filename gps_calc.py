#!/usr/bin/env python2

import math
from haversine import haversine
from pprint import pprint as pp
import logging


'''
Class: ao_geolocation_notifier

This class keep tracks of distanecs between our network and dsrc.
It will notify uppler level modules when distances are updated.
'''

class ao_geolocation_notifier():
	'''
	Initilization method:
	Initializes instances, provide distance filter threshold.
	'''
	def __init__(self, threshold):
		self._threshold = threshold
		self._return_list = []
		self._my_network_list = []
		self._dsrc_network_list = []
		self._is_ready = False
		self._distance_changed = True
		try:
			self._logger = logging.getLogger('AirOrange-GPS-Notifier')
			self.initialize_logger()
			self._logger.debug("Logger Initialized.")
		except Exception as e:
			print("Logger Init. Failed, log might not be available")

	'''
	initialize main logger, REMEMBER to change log level
	'''
	def initialize_logger(self):
		self._logger.setLevel(logging.DEBUG)
		self._log_console_handler = logging.StreamHandler()
		self._log_file_handler = logging.FileHandler('/logs/gps.log')

		#Change logging level for production
		self._log_file_handler.setLevel(logging.DEBUG)
		self._log_console_handler.setLevel(logging.ERROR)

		self._formatter_screen = logging.Formatter('\x1b[1;37;44m[%(asctime)s] in function:[%(funcName)s]LINE %(lineno)d:\n\x1b[0m %(message)s')
		self._formatter_file = logging.Formatter('[%(asctime)s]-[%(levelname)8s] [%(module)s]->%(threadName)s in function:[%(funcName)s]LINE %(lineno)d:\n %(message)s')
		
		self._log_file_handler.setFormatter(self._formatter_file)
		self._log_console_handler.setFormatter(self._formatter_screen)
		self._logger.addHandler(self._log_console_handler)
		self._logger.addHandler(self._log_file_handler)

	'''
	update both coordinate_lists ,redundant function
	'''
	def update_coordinate_lists (self,self_coord_list, dsrc_coord_list):
		self._logger.debug("attempting to update both coordinate list")
		self._is_ready = False
		self._my_network_list = self_coord_list
		self._dsrc_network_list = dsrc_coord_list
		self._logger.info("my and dsrc list updated.")
		self._logger.debug("\nmy:\n{}\n\ndsrc:\n{}".format(repr(self._my_network_list),repr(self._dsrc_network_list)))
		self.refresh_distance_map()
		self._logger.info("Distance map refreshed.")
		self._logger.debug("\nMap:\n{}".format(repr(self._return_list)))

	'''
	update my coordinate_lists only
	'''
	def update_my_coordinate_list (self,self_coord_list):
		self._logger.debug("attempting to update my coordinate list")
		self._is_ready = False
		self._my_network_list = self_coord_list
		self._logger.info("my list updated.")
		self._logger.debug("\nmy:\n{}\n".format(repr(self._my_network_list)))
		self.refresh_distance_map()
		self._logger.info("Distance map refreshed.")
		self._logger.debug("\nMap:\n{}".format(repr(self._return_list)))

	'''
	update dsrc coordinate_lists only
	'''
	def update_dsrc_coordinate_list (self,dsrc_coord_list):
		self._logger.debug("attempting to update dsrc coordinate list")
		self._is_ready = False
		self._dsrc_network_list = dsrc_coord_list
		self._logger.info("dsrc list updated.")
		self._logger.debug("\ndsrc:\n{}".format(repr(self._dsrc_network_list)))
		self.refresh_distance_map()
		self._logger.info("Distance map refreshed.")
		self._logger.debug("\nMap:\n{}".format(repr(self._return_list)))

	'''
	Set distance threshold, -1 case return everything
	'''

	def set_threshold(self, threshold):
		self._logger.debug("attempting to set threshold, old:{} new{}".format(self._threshold,threshold))
		if(threshold < 0):
			self._threshold = -1
			self._logger.info("Threshold set to return all DSRC nodes")
			self.refresh_distance_map()
		elif (threshold>=0):
			self._threshold = threshold
			self._logger.info("Threshold set {}".format(self._threshold))
			self.refresh_distance_map()

	'''
	Recalculate map based on new coordinates, not delta update yet
	'''
	def refresh_distance_map(self):
		self._logger.info("Attempting to refreshed distance map.")
		try:
			self._is_ready = False
			self._return_list = self.get_distance_update(self._my_network_list,self._dsrc_network_list,self._threshold)
			self._is_ready = True
			self._distance_changed = True
		except Exception as e:
			self._is_ready = False
			self.logger.error("WARNING: Exception: {} when attempting to update DSRC distance map!".format(repr(e)))

	'''
	Return requested map, if ready.
	'''

	def get(self):
		self._logger.info("Get() Request Recieved.")
		if self._is_ready:
			self._logger.info("Returning map")
			self._logger.debug("Returning:\n{}".format(repr(self._return_list)))
			return self._return_list
		else:
			self._logger.error("Map is NOT ready or unchagned since last pull. Wait and try again")
			return []


	'''
	build distance map list
	'''
	def get_distance_update (self, self_coord_list, dsrc_coord_list, threshold = -1):
		self._logger.debug("Calculating distance with filtring threshold {}".format(threshold))
		distance_map = dict()
		for my_node in self_coord_list:
			my_node_id = my_node[0]
			dsrc_list = []
			for dsrc_node in dsrc_coord_list:
				drsc_node_id = dsrc_node[0]
				distance_to_dsrc_node = self.get_distance(my_node[1],my_node[2],my_node[3],dsrc_node[1],dsrc_node[2],dsrc_node[3])
				if threshold == -1:
					dsrc_list.append([drsc_node_id,distance_to_dsrc_node])
				elif(distance_to_dsrc_node < threshold):
					dsrc_list.append([drsc_node_id,distance_to_dsrc_node])
				#by default returns nothing
			if dsrc_list:
				distance_map.update({my_node_id:dsrc_list})
		return distance_map

	'''
	Calculate actual distance given two coordinates
	'''

	def get_distance(self,ref_longitude,ref_latitude, ref_altitude, tgt_longitude,tgt_latitude,tgt_altitude):
		a = (ref_longitude, ref_latitude)
		b = (tgt_longitude, tgt_latitude)
		flat_distance = haversine(a,b) #unit in km
		flat_distance	= flat_distance	*1000.0
		los_dist = math.sqrt( (ref_altitude - tgt_altitude)**2 + flat_distance**2)
	
		return los_dist


if __name__ == "__main__":
		#Sample Data
	print("Testing GPS parser......")
	loc_1 = [(9, 35.9879493713, -86.7912979126, -0.842000007629), 
	(7, 35.9921951294, -86.7892303467, 2.64700007439), 
	(6, 35.9955101013, -86.78490448, 0.65600001812), 
	(5, 35.9954414368, -86.7851257324, 1.2990000248), 
	(4, 35.9922485352, -86.7965774536, 3.9319999218), 
	(3, 35.9923782349, -86.7965011597, 2.1819999218), 
	(10, 35.9894714355, -86.7783660889, 0.973999977112), 
	(2, 35.9894447327, -86.7783508301, 2.75900006294), 
	(1, 35.992023468, -86.7893295288, -0.222000002861)]
	
	loc_2 = [(143, 35.997913333, -86.7985, 2.21), 
	(142, 35.998316667, -86.798646667, 2.251),
	 (144, 35.964301667, -86.787021667, 3.04),
	 (145, 35.964473333, -86.788826667, 0.794)]

	loc_3 =[(9, 35.9879646301, -86.7912826538, 2.21499991417),
(8, 35.9880752563, -86.7910995483, 3.21300005913),
(7, 35.9921836853, -86.7892074585, 3.14700007439),
(6, 35.9955101013, -86.7849197388, 1.58800005913),
(5, 35.9954528809, -86.785118103, 2.04200005531),
(4, 35.9922180176, -86.7965545654, 2.33899998665),
(3, 35.9923973083, -86.7964859009, -0.070000000298),
(10, 35.9894981384, -86.7783508301, 2.48799991608),
(2, 35.9894332886, -86.7783508301, 2.35500001907),
(1, 35.9920272827, -86.7893295288, 1.31599998474)]
 	
 	loc_4= [(143, 35.995765, -86.781246667, 0.073),
(144, 35.965875, -86.80387, 3.23),
(145, 35.96592, -86.805436667, 2.247),
(142, 35.995831667, -86.781765, 1.346)]
	print("Data Set 1:\n")
	print("Requesting nodes within 2km")
	geo_notif = ao_geolocation_notifier(2000)
	geo_notif.update_coordinate_lists(loc_1,loc_2)
	pp(geo_notif.get())
	print("\nRequesting nodes within 1km")
	geo_notif.set_threshold(1000)
	pp(geo_notif.get())
	print("\nRequesting all nodes")
	geo_notif.set_threshold(-1)
	pp(geo_notif.get())
	print("Update to use anothe dsrc coordinates group")
	geo_notif.update_dsrc_coordinate_list(loc_4)
	pp(geo_notif.get())
	print("Switching to data set 2")
	geo_notif.update_my_coordinate_list(loc_3)
	geo_notif.update_dsrc_coordinate_list(loc_4)
	print("Requesting nodes within 2km")
	geo_notif.set_threshold(2000)
	pp(geo_notif.get())
	print("\nRequesting nodes within 1km")
	geo_notif.set_threshold(1000)
	pp(geo_notif.get())
	print("\nRequesting all nodes")
	geo_notif.set_threshold(-1)
	pp(geo_notif.get())